package cn.ms.light.type;

public enum ReqType {
	
	ALL("ALL"),
	
	CONNECT("CONNECT"),
	
	DELETE("DELETE"),
	
	GET("GET"),
	
	HEAD("HEAD"),
	
	OPTIONS("OPTIONS"),
	
	PATCH("PATCH"),
	
	POST("POST"),
	
	PUT("PUT"),
	
	TRACE("TRACE");
	
	String name;
	
	ReqType(String name) {
		this.name=name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
