package cn.ms.light.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import cn.ms.light.type.DataType;
import cn.ms.light.type.ReqType;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Handler {

	String value();
	
	ReqType reqType() default ReqType.ALL;
	
	DataType dataType() default DataType.TEXT;
	
	String contentType() default "text/plain;charset=utf-8";
	
	String charset() default "utf-8";
	
}
