package cn.ms.light.core;

import java.util.Map;

import io.reactivex.netty.channel.Handler;
import io.reactivex.netty.protocol.http.server.HttpServerRequest;
import io.reactivex.netty.protocol.http.server.HttpServerResponse;
import rx.Observable;

/**
 * creates a Handler that comes with matched URL params
 */
public interface Route<I,O> extends Handler<HttpServerRequest<I>, HttpServerResponse<O>> {

   Observable<Void> handle(Map<String, String> params, HttpServerRequest<I> request, HttpServerResponse<O> response);
   
}
